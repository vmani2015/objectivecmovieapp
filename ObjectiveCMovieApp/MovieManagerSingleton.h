//
//  MovieManagerSingleton.h
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 16/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface MovieManagerSingleton : NSObject

- (void)getAllShortPlots:(void (^)(NSError*))completionBlock;

- (void)getAllLongPlots:(void (^)(NSError*))completionBlock;

- (void)downloadAllImages:(void (^)(NSError*))completionBlock;

-(void) getMovies :(NSString*) input completion:(void (^)(NSError*))completionBlock;

-(NSString *) convertAccentedStringtoNormalString:(NSString *) input;

-(void) BooleanToNSLog:(BOOL) input;

-(BOOL) isLastCharacterSpace :(NSString*) input ;


@property (nonatomic, retain) NSString *BaseSearchURLStringStart;

@property (nonatomic, retain) NSString *TitleSearchStringStart;

@property (nonatomic, retain) NSString *ShortPlotStringEnd;

@property (nonatomic, retain) NSString *LongPlotStringEnd;

@property (nonatomic, retain) NSMutableArray *current_movie_array;

+ (id)sharedManager;



@property (nonatomic, retain) dispatch_queue_t search_queue;
@property (nonatomic, retain) dispatch_group_t search_group;

@property (nonatomic, retain) NSString *Saved_Search_Query;




@end
