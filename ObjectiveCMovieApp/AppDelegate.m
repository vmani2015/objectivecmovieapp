//
//  AppDelegate.m
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 15/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import "AppDelegate.h"


@interface AppDelegate ()



@end

@implementation AppDelegate

//Used to keep track of when the application is monitoring.
BOOL isMonitoring = false;

//This is to keep a track of the internet connection and when it changes.
BOOL isConnected = false;

//Keep track of the current table view controller
MovieTableViewController* current_table_view_controller;


-(BOOL)GetConnectionStatus
{
  return isConnected;
}

-(void)SetMovieTableViewController:(MovieTableViewController *)tvc
{
  current_table_view_controller = tvc;
}

-(void)SetSavedQueryAlertBox:(NSString *)saved_query
{
  //Now we first need to ask the user if they want to search
  
  NSString *combined_String = [[@"Would you like to search with the saved query of '" stringByAppendingString:saved_query ]stringByAppendingString:@"' ? This will replace your existing results."];
  
  UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert" message:combined_String preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
    
    [current_table_view_controller searchForMovies:saved_query];
    
  }];
  UIAlertAction *otherAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
    
    NSLog(@"Query search cancelled");
    
  }];
  [alert addAction:defaultAction];
  [alert addAction:otherAction];
  
  //Present action where needed
  [current_table_view_controller presentViewController:alert animated:YES completion:nil];
}


//-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions
//{
//    NSLog(@"This prints here");
//    [NSThread sleepForTimeInterval:5.0];
//    
//    return YES;
//}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  // Override point for customization after application launch.
  
    
//    [NSThread sleepForTimeInterval:5.0];
    
  [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
    NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
    
    //Now we change the isConnected Flag based on the status
    //This means either connected by wifi.
    
   
    if (status > 1)
    {
      isConnected = true;
    
      //Now we process the saved query
    
      NSString *saved_query = [[MovieManagerSingleton sharedManager] Saved_Search_Query];
    
      if ([saved_query isEqualToString:@""] == false)
        {
        
          NSLog(@"The saved query is %@", saved_query);
        
          [self SetSavedQueryAlertBox:saved_query];
        
        
        }
      
    }
    else
    {
      isConnected = false;
    }
    
  }];
  
  
  if (isMonitoring == false)
    {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    NSLog(@"Monitoring Started 1");
    isMonitoring = true;
    }
  
  
  
  return NO;
}


- (void)applicationWillResignActive:(UIApplication *)application {
  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
  // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  
  if (isMonitoring == true)
    {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    NSLog(@"Monitoring Stopped 1");
    isMonitoring = false;
    }
  
  
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  
  if (isMonitoring == true)
  {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    NSLog(@"Monitoring Stopped 2");
    isMonitoring = false;
  }
  
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
  // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  
  if (isMonitoring == false)
    {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    NSLog(@"Monitoring Started 2");
    isMonitoring = true;
    }
 
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
  // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  
  if (isMonitoring == false)
  {
  [[AFNetworkReachabilityManager sharedManager] startMonitoring];
  NSLog(@"Monitoring Started 3");
  isMonitoring = true;
  }
  
  
}


- (void)applicationWillTerminate:(UIApplication *)application {
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

  if (isMonitoring == true)
    {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
    NSLog(@"Monitoring Stopped 3");
    isMonitoring = false;
    }
  
  
}


@end
