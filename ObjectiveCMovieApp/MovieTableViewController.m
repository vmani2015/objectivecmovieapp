//
//  MovieTableViewController.m
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 15/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import "MovieTableViewController.h"
#import "MovieTableViewCell.h"
#import "MovieManagerSingleton.h"
#import "Movie.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
//#import "AFNetworking.h"
//#import "UIImageView+AFNetworking.h"
#import "MBProgressHUD.h"
#import "MovieDetailViewController.h"


@interface MovieTableViewController ()



@end

@implementation MovieTableViewController


MovieManagerSingleton *sharedManager;

AppDelegate *appDelegate ;

BOOL error_received;

int selected_movie_row;

BOOL user_cancelled_search;


- (void)viewDidLoad {
    [super viewDidLoad];
  
  
  sharedManager = [MovieManagerSingleton sharedManager];
  
  appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  
  error_received = false;
  
  selected_movie_row = 0;
  
  user_cancelled_search = false;
  
  
  
  
  
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [[sharedManager current_movie_array] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    // Configure the cell...
  
  NSString *CellIdentifier = @"MovieTableViewCell";
  
  MovieTableViewCell *cell = (MovieTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil)
    {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
    cell = [nib objectAtIndex:0];
    }
  
  Movie *m = [[sharedManager current_movie_array] objectAtIndex:indexPath.row];
  
  cell.title.text = [m title];
  
  cell.short_plot.text = [m short_plot];
  
  cell.year.text = [m year];
  
  cell.image_view.image = [m poster_image];
  
  
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return 150;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  NSLog(@" The movie selected is %ld",(long)indexPath.row);
  
  [self performSegueWithIdentifier:@"movie_detail_segue" sender:self];

}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
  //We want to clear all of the text if there is nothing in it.
  if ( [searchText length] == 0)
    {
    [[sharedManager current_movie_array] removeAllObjects];
    [[self tableView] reloadData];
    
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [self showAlertViewMessage:@"Error" Alert_Message:@"The OMDB API is no longer free. Please find an alternative API to demonstrate this."];
    
    //The OMDB API has changed and is no longer free.
  //[self searchForMovies:searchBar.text];
}

-(void)showAlertViewMessage:(NSString *)Alert_Title Alert_Message:(NSString *)input2
{
  
  UIAlertController *alert = [UIAlertController alertControllerWithTitle:Alert_Title message:input2 preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
    
    //Print the search string to the console.
    NSLog(@"%@", input2);
    
    
    
  }];
  [alert addAction:defaultAction];
  //Present action where needed
  [self presentViewController:alert animated:YES completion:nil];
  
}

-(void)singleTap:(UITapGestureRecognizer*)sender
{
  
  
  
  NSLog(@"Search Cancelled");
  
  //Show a message to the user that the search has been cancelled
  [self showAlertViewMessage:@"Information" Alert_Message:@"Search Cancelled"];
  
  //Set the User Cancelled Flag to TRUE
  user_cancelled_search = TRUE;
  
  //Hide the HUD now that the search is cancelled
  sender.view.hidden = TRUE;
  
  //We should also now clear the array of any elements.
  [[sharedManager current_movie_array] removeAllObjects];
  
  //Remove the text from the Search bar
  _searchBar.text = @"";
  
}



//Custom functions

-(void)searchForMovies:(NSString *)search_query
{
    
    //Remove all of the spaces first from the search query
    NSString *current_search_query = [search_query stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    //Check if the query is valid first before checking for the internet connection.
    if ([current_search_query isEqualToString:@""])
    {
        NSString* error_string = @"Please enter some text to search for a movie";
        
        
        [self showAlertViewMessage:@"Error" Alert_Message:error_string];
        
        [_searchBar setText:@""];
    }
    else if ([appDelegate GetConnectionStatus] == false )
    {
    
    NSString* error_string = @"Unable to Connect to Internet. Your search query has been saved";
    
    
    [self showAlertViewMessage:@"Error" Alert_Message:error_string];
    
    
    NSLog(@"%@",error_string);
    
    //We now save the query string
    sharedManager.Saved_Search_Query = search_query;
    
    //We also send the movie controller to the app delegate
    [appDelegate SetMovieTableViewController:self];
    
    }
  else
    {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode =  MBProgressHUDModeIndeterminate;
    hud.label.text = @"Loading...";
    hud.detailsLabel.text = @"Click to cancel search";
    
    UITapGestureRecognizer *HUDSingleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTap:)];
    [hud addGestureRecognizer:HUDSingleTap];
    
    
    
    //Activate the progress HUD
    
    [hud showAnimated:YES];
    
    
    //We also need to dismiss the keyboard from the view
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    
    
    //We need to add the remaining code to this
    NSString *search_text = search_query;
    
    //If the last character of the string is " ", we need to take the rest of the string
    
    if ([sharedManager isLastCharacterSpace:search_text] == true)
      {
      search_text = [search_text substringToIndex:[search_text length] - 1];
      }
    
    //The we replace the rest of the " " with "+"
    search_text = [search_text stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    
    
    
    //We first want to show the progress bar
    //  [_ProgressBar setHidden:false];
    
    //Clear the array if there is anything in it
    
    [[sharedManager current_movie_array] removeAllObjects];
    
    //Update the progress of the HUD
    [hud setProgress:0.1];
    
    
    
    [sharedManager getMovies:search_text completion:^(NSError * error)
    {
     
      if (error != nil && user_cancelled_search == false)
      {
        //Log that there was an error.
        NSLog(@"Internet Connection Interuppted 1");
      
      //Hide the progress HUD
      [hud hideAnimated:true];
      
      //Alert the user that there was an error
      [self showAlertViewMessage:@"Error" Alert_Message:@"Internet Connection Interrupted 1. Please make sure that you have a stable connection and then try again"];
      
      //Clear the Movie Array Singleton - this will prevent partial results from bring loaded
      [[sharedManager current_movie_array] removeAllObjects];
      
      //Set the error recieved flag to true
      error_received = true;
      
      
      
      }
      else
      {
      
      
        //Update the progress of the HUD
        [hud setProgress:0.4];
     
        if ( error_received == false && user_cancelled_search == false)
        {
        
        [sharedManager downloadAllImages:^(NSError * error)
         {
      
            //We need to check if there was an error here
            if (error != nil)
              {
              //Log that there was an error.
              NSLog(@"Internet Connection Interuppted 4");
          
              //Hide the progress HUD
              [hud hideAnimated:true];
          
              //Alert the user that there was an error
              [self showAlertViewMessage:@"Error" Alert_Message:@"Internet Connection Interrupted 4. Please make sure that you have a stable connection and then try again"];
          
              //Clear the Movie Array Singleton - this will prevent partial results from bring loaded
              [[sharedManager current_movie_array] removeAllObjects];
          
              //Set the error recieved flag to true
              error_received = true;
              }
            else
              {
           
                  //Get all of the short plots
      
                  [sharedManager getAllShortPlots:^(NSError * error)
                   {
      
                       if (error != nil && user_cancelled_search == false)
                       {
                           //Log that there was an error.
                           NSLog(@"Internet Connection Interuppted 2");
                
                           //Hide the progress HUD
                           [hud hideAnimated:true];
                
                           //Alert the user that there was an error
                           [self showAlertViewMessage:@"Error" Alert_Message:@"Internet Connection Interrupted 2. Please make sure that you have a stable connection and then try again"];
                
                           //Clear the Movie Array Singleton - this will prevent partial results from bring loaded
                           [[sharedManager current_movie_array] removeAllObjects];
                
                           //Set the error recieved flag to true
                           error_received = true;
                       }
                       else
                       {
        
                           if ( error_received == false && user_cancelled_search == false)
                           {
                  
                               //Get all of the long plots
        
                               [sharedManager getAllLongPlots:^(NSError * error)
                                {
        
                                    if (error != nil)
                                    {
                                        //Log that there was an error.
                                        NSLog(@"Internet Connection Interuppted 3");
                          
                                        //Hide the progress HUD
                                        [hud hideAnimated:true];
                                        
                                        //Alert the user that there was an error
                                        [self showAlertViewMessage:@"Error" Alert_Message:@"Internet Connection Interrupted 3. Please make sure that you have a stable connection and then try again"];
                          
                                        //Clear the Movie Array Singleton - this will prevent partial results from bring loaded
                                        [[sharedManager current_movie_array] removeAllObjects];
                          
                                        //Set the error recieved flag to true
                                        error_received = true;
                                    }
                                    else
                                    {
          
                                        if ( [[sharedManager current_movie_array] count] != 0)
                                        {
        
        
                                            //Update the progress of the HUD
                                            [hud setProgress:0.7];
        
        
                                            [[self tableView] reloadData];
        
                                            //Update the progress of the HUD
                                            [hud setProgress:1.0];
        
                                            //Dismiss the HUD
                                            [hud hideAnimated:TRUE];
        
                                        }
                                        else
                                        {
        
                                            //Dismiss the HUD
                                            [hud hideAnimated:TRUE];
        
                                            NSString* error_string = [@"There are no results for " stringByAppendingString:search_text];
        
        
                                            [self showAlertViewMessage:@"Error" Alert_Message:error_string];
        
                                        }
                          
                                    }
                            }];
                        }
        
                }
      
            }];
        }
      
      }];
      
      }
      
      }
     
     }];
    
    
    
    }
  
  
  
}







/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
  
  
  
  if ( [segue.identifier isEqualToString:@"movie_detail_segue"] )
    {
  
  
    
    
    /*This is to debug the code when we are having errors*/
    
   
  
    MovieDetailViewController *mdvc = (MovieDetailViewController *)segue.destinationViewController;
  
    
    mdvc.movie_selected =  [[sharedManager current_movie_array] objectAtIndex:
                            [[[self tableView] indexPathForSelectedRow] row]];
    
    

    
    }
  
}




@end
