//
//  LaunchScreenViewController.h
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 2/5/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaunchScreenViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *MovieTitleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *MovieImage;

@property (weak, nonatomic) IBOutlet UILabel *MovieFullPlot;

@property Movie* movie_selected;

@property (weak, nonatomic) IBOutlet UIScrollView *MovieDetailScrollView;

@end
