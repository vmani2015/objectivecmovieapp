//
//  MovieManagerSingleton.m
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 16/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import "MovieManagerSingleton.h"
#import "AFNetworking.h"
#import "Movie.h"
#import "AFImageDownloader.h"

@implementation MovieManagerSingleton



@synthesize BaseSearchURLStringStart;
@synthesize TitleSearchStringStart;
@synthesize ShortPlotStringEnd;
@synthesize LongPlotStringEnd;
@synthesize current_movie_array;
@synthesize search_queue;
@synthesize search_group;
@synthesize Saved_Search_Query;



+ (id)sharedManager {
  static MovieManagerSingleton *sharedMyManager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedMyManager = [[self alloc] init];
  });
  return sharedMyManager;
}

- (id)init {
  if (self = [super init]) {
    BaseSearchURLStringStart = @"http://www.omdbapi.com/?s=";
    TitleSearchStringStart = @"http://www.omdbapi.com/?t=";
    ShortPlotStringEnd = @"&y=&plot=short&r=json";
    LongPlotStringEnd = @"&y=&plot=full&r=json";
    current_movie_array = [[NSMutableArray alloc] init];
    search_queue = dispatch_queue_create("My first custom queue", DISPATCH_QUEUE_SERIAL);
    search_group = dispatch_group_create();
    Saved_Search_Query = @"";
  }
  return self;
}


-(void)BooleanToNSLog:(BOOL)input
{
  NSLog(@"%@",input ? @"YES" : @"NO");
}

-(NSString *)convertAccentedStringtoNormalString:(NSString *)input
{
  return [input stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale systemLocale]];
}




-(BOOL)isLastCharacterSpace:(NSString *)input
{
  
  NSString *newString = [input substringFromIndex:[input length]-1];
  
  BOOL result = false;

  if ([newString isEqualToString:@" "])
    {
    result = true;
    }
  
  return result;
}

-(void)getAllShortPlots:(void (^)(NSError *))completionBlock
{
  
 
  
  
  dispatch_sync(search_queue, ^{
    
    NSString *input_String = TitleSearchStringStart;
    
    NSString *combined_String;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    NSString *formatted_title_string;
    
    
    
    
    for (Movie* m in current_movie_array)
      {
      
      dispatch_group_enter(search_group);
      
      //We first need to change all " " in the title to "+"
      formatted_title_string = [m.title stringByReplacingOccurrencesOfString:@" " withString:@"+"];
      
      //Reset the combined string in each loop.
      combined_String = @"";
      
      combined_String = [[input_String stringByAppendingString:formatted_title_string] stringByAppendingString:ShortPlotStringEnd];
      
      NSLog(@"Combined String is %@",combined_String);
      
      
      
      [manager GET:combined_String parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
       {
       
       NSLog(@"---------------------");
       NSLog(@"The current plot is %@",[responseObject objectForKey:@"Plot"]);
       NSLog(@"---------------------");
       
       m.short_plot = [responseObject objectForKey:@"Plot"];
       
       dispatch_group_leave(search_group);
       
       } failure:^(NSURLSessionTask *operation, NSError *error)
       {
        NSLog(@"Error: %@", error);
        dispatch_group_leave(search_group);
        completionBlock(error);
       
       }];
      
      
      
      
      }

    
    
    dispatch_group_notify(search_group, dispatch_get_main_queue(), ^{
      //After all of the short plots have downloaded.
      completionBlock(nil);
    });
    
    
    
  });
  

}


-(void)getAllLongPlots:(void (^)(NSError *))completionBlock
{
  
  
  
  
  dispatch_sync(search_queue, ^{
    
    NSString *input_String = TitleSearchStringStart;
    
    NSString *combined_String;
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    NSString *formatted_title_string;
    
    
    
    
    for (Movie* m in current_movie_array)
      {
      
      dispatch_group_enter(search_group);
      
      //We first need to change all " " in the title to "+"
      formatted_title_string = [m.title stringByReplacingOccurrencesOfString:@" " withString:@"+"];
      
      //Reset the combined string in each loop.
      combined_String = @"";
      
      combined_String = [[input_String stringByAppendingString:formatted_title_string] stringByAppendingString:LongPlotStringEnd];
      
      NSLog(@"Combined String is %@",combined_String);
      
      
      
      [manager GET:combined_String parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
       {
       
       NSLog(@"---------------------");
       NSLog(@"The current plot is %@",[responseObject objectForKey:@"Plot"]);
       NSLog(@"---------------------");
       
       m.long_plot = [responseObject objectForKey:@"Plot"];
       
       dispatch_group_leave(search_group);
       
       } failure:^(NSURLSessionTask *operation, NSError *error)
       {
       NSLog(@"Error: %@", error);
       dispatch_group_leave(search_group);
       completionBlock(error);
       }];
      
      
      
      
      }
    
    
    
    dispatch_group_notify(search_group, dispatch_get_main_queue(), ^{
      //After all of the long plots have downloaded.
      completionBlock(nil);
    });
    
    
    
  });
  
  
}



-(void)downloadAllImages:(void (^)(NSError *))completionBlock
{
  //As the movies array is a singleton. We have to implement this here.
    
  dispatch_sync(search_queue, ^{
    
    
    
    for (Movie* m in current_movie_array)
      {
      
        dispatch_group_enter(search_group);
      
      
        NSLog(@"---------------------------------");
        NSLog(@"Current Movie Title: %@",m.title);
        NSLog(@"Current Movie URL: %@",m.image_URL);
        NSLog(@"---------------------------------");
      
        //If the image URL is N/A, we need to process this.
      
        if ([m.image_URL isEqualToString:@"N/A"] == true)
          {
        
            m.poster_image = [UIImage imageNamed:@"no_image_found_cut.png"];
        
            dispatch_group_leave(search_group);
        
          }
        else
          {
            NSURL *current_image_url = [NSURL URLWithString:m.image_URL];
        
            NSError* error = [[NSError alloc] init];
        
            NSData *current_image_data = [NSData dataWithContentsOfURL:current_image_url];
        
        
            if ( current_image_data  == nil)
              {
                dispatch_group_leave(search_group);
                completionBlock(error);
                break;
              }
            else
              {
                UIImage *current_image = [UIImage imageWithData:current_image_data];
                m.poster_image = current_image;
                dispatch_group_leave(search_group);
              }
            }
      }
    
    
    dispatch_group_notify(search_group, dispatch_get_main_queue(), ^{
      //After all of the images have downloaded
      completionBlock(nil);
    });
    
  });
  
}


-(void)getMovies:(NSString *)input completion:(void (^)(NSError *))completionBlock
{
  
  dispatch_sync(search_queue, ^{
    
    dispatch_group_enter(search_group);

    
    NSString *input_String = [BaseSearchURLStringStart stringByAppendingString:input];
    
    NSString *combined_String = [input_String stringByAppendingString:ShortPlotStringEnd];
    
    NSLog(@"Combined String is %@",combined_String);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:combined_String parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
      
      //Loop through all of the NSDictionary
      
      //Make sure that the object is not null
      
      NSString* response_valid = [responseObject objectForKey:@"Response"];
      
      
      
      if ( [response_valid isEqualToString:@"True"])
        {
        
        //We need to count the number of responses
        int result_count = [[responseObject objectForKey:@"totalResults"] intValue];
        int counter = 0;
        
        //The JSON only has at max 10 results so we need to set the result_count to 10 if the result_count is greater than 10
        if ( result_count > 0)
        {
          result_count = 10;
        }
        
        Movie* current_movie;
        
        while (counter < result_count)
          {
          
          current_movie = [[Movie alloc] init];
          
          
          //This is used to convert characters diacritic title strings to normal character strings.
          
           current_movie.title = [self convertAccentedStringtoNormalString:[responseObject objectForKey:@"Search"][counter][@"Title"]];
          
          current_movie.m_id = [responseObject objectForKey:@"Search"][counter][@"imdbID"];
          
          current_movie.image_URL = [responseObject objectForKey:@"Search"][counter][@"Poster"];
          
          current_movie.year = [responseObject objectForKey:@"Search"][counter][@"Year"];
          
          [current_movie_array addObject:current_movie];
          
         
          counter++;
          }
      
        }
      
      dispatch_group_leave(search_group);
      
      dispatch_group_notify(search_group, dispatch_get_main_queue(), ^{
        //After all of the images have downloaded
        completionBlock(nil);
      });
      

    } failure:^(NSURLSessionTask *operation, NSError *error) {
      NSLog(@"Error: %@", error);
      completionBlock(error);
    }];
    
    
    NSLog(@"The search string is %@",input);
    
  });


  
  
 
}


@end
