//
//  Movie.h
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 15/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Movie : NSObject

@property NSString *m_id;
@property NSString *title;
@property NSString *short_plot;
@property NSString *long_plot;
@property NSString *image_URL;
@property NSString *year;
@property UIImage *poster_image;



@end
