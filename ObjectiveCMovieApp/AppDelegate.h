//
//  AppDelegate.h
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 15/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFNetworking.h"
#import "MovieManagerSingleton.h"
#import "MovieTableViewController.h" 

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


-(BOOL)GetConnectionStatus;

-(void)SetMovieTableViewController:(MovieTableViewController *) tvc;

-(void)SetSavedQueryAlertBox:(NSString *) saved_query;
  

@end

