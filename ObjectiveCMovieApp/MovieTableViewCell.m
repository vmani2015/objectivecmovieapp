//
//  MovieTableViewCell.m
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 16/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import "MovieTableViewCell.h"

@implementation MovieTableViewCell

@synthesize title = _title;

@synthesize year = _year;

@synthesize image_view = _image_view  ;

@synthesize short_plot = _short_plot;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
