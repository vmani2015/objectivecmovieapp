//
//  MovieDetailViewController.m
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 1/12/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import "MovieDetailViewController.h"
#import "Movie.h"
#import "MovieManagerSingleton.h"

@interface MovieDetailViewController ()


@end


@implementation MovieDetailViewController

@synthesize MovieTitleLabel;
@synthesize MovieFullPlot;
@synthesize MovieImage;
@synthesize movie_selected;
@synthesize MovieDetailScrollView;



- (void)viewDidLoad {
  
  [super viewDidLoad];
  
  // Do any additional setup after loading the view.
  [[[self navigationController] navigationBar] setTranslucent:false];
  
  [[MovieManagerSingleton sharedManager] BooleanToNSLog:[self automaticallyAdjustsScrollViewInsets]];
  
//  [self setAutomaticallyAdjustsScrollViewInsets:<#(BOOL)#>];
  
  [[MovieManagerSingleton sharedManager] BooleanToNSLog:[self automaticallyAdjustsScrollViewInsets]];
  
  self.edgesForExtendedLayout = UIRectEdgeNone;
  
  MovieTitleLabel.text = [movie_selected title];
  MovieFullPlot.text = [movie_selected long_plot];
  MovieImage.image = [movie_selected poster_image];
  
  
  
 

  
 
}

-(CGRect)rectForText:(NSString *)text
           usingFont:(UIFont *)font
       boundedBySize:(CGSize)maxSize
{
  NSAttributedString *attrString =
  [[NSAttributedString alloc] initWithString:text
                                  attributes:@{ NSFontAttributeName:font}];
  
  return [attrString boundingRectWithSize:maxSize
                                  options:NSStringDrawingUsesLineFragmentOrigin
                                  context:nil];
}

-(void)viewDidAppear:(BOOL)animated
{
  
  CGSize maximumLabelSize = CGSizeMake(280,9999);
  CGRect titleRect = [self rectForText:MovieFullPlot.text // <- your text here
                             usingFont:MovieFullPlot.font
                         boundedBySize:maximumLabelSize];
  
  CGFloat current_height = MovieFullPlot.frame.origin.y + titleRect.size.height;
  CGFloat frame_height = self.view.frame.size.height;
  
  
  NSLog(@"The current height is : %f",current_height);
  NSLog(@"The frame height is : %f", frame_height);
  
  
//  self.view.frame.size.height;
//  + fabs(titleRect.size.height);
  
  if (current_height < frame_height)
    {
    [MovieDetailScrollView setContentSize:CGSizeMake(self.view.frame.size.width,frame_height + (frame_height- current_height)  )];
    }
  else
    {
    [MovieDetailScrollView setContentSize:CGSizeMake(self.view.frame.size.width,current_height  )];
    }
  
  

  
 
  
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
