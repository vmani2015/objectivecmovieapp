//
//  MovieTableViewCell.h
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 16/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *title;

@property (nonatomic, weak) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UILabel *short_plot;

@property (nonatomic, weak) IBOutlet UIImageView *image_view;
@end
