//
//  MovieTableViewController.h
//  ObjectiveCMovieApp
//
//  Created by Vinu Mani on 15/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieTableViewController : UITableViewController<UISearchBarDelegate,UISearchControllerDelegate>



@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;


-(void)showAlertViewMessage:(NSString *) Alert_Title Alert_Message:(NSString *) input2;

-(void)searchForMovies:(NSString *) search_query;






@end


