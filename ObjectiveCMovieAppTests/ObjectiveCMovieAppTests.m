//
//  ObjectiveCMovieAppTests.m
//  ObjectiveCMovieAppTests
//
//  Created by Vinu Mani on 15/11/2016.
//  Copyright © 2016 VimanEnterprises. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AFNetworking.h"
#import "Movie.h"

@interface ObjectiveCMovieAppTests : XCTestCase

@end

@implementation ObjectiveCMovieAppTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testConvertAccentedStringtoNormalString
{
  
  NSString *input = @"Près de 60% d'électeurs ont rejeté par référendum";
  
  NSString *converted_string = [input stringByFoldingWithOptions:NSDiacriticInsensitiveSearch locale:[NSLocale systemLocale]];
  
  NSString *exptected_result = @"Pres de 60% d'electeurs ont rejete par referendum";

  
  XCTAssertTrue([converted_string isEqualToString:exptected_result]);
}

- (void)testBooleanToNSLog {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
  
      Boolean input = true;
  
  
      XCTAssertEqual(@"YES",input ? @"YES" : @"NO");
  
    Boolean input2 = false;
  
  
  XCTAssertEqual(@"NO", input2 ? @"YES" : @"NO");
}


-(void)testIsLastCharacterSpace
{
  
  NSString *input1 = @"The lord of the ";
  
  NSString *newString1 = [input1 substringFromIndex:[input1 length]-1];
  
  BOOL result1 = false;
  
  if ([newString1 isEqualToString:@" "])
    {
    result1 = true;
    }
  
  XCTAssertTrue(result1);
  
  NSString *input2 = @"Harry potter";
  
  NSString *newString2 = [input2 substringFromIndex:[input2 length]-1];
  
  BOOL result2 = false;
  
  if ([newString2 isEqualToString:@" "])
    {
    result2 = true;
    }
  
  XCTAssertFalse(result2);
  
  
}

-(void)testGetMovies
{
  
  dispatch_queue_t search_queue = dispatch_queue_create("My first custom queue", DISPATCH_QUEUE_SERIAL);
  dispatch_group_t search_group = dispatch_group_create();
  
  NSString *BaseSearchURLStringStart = @"http://www.omdbapi.com/?s=";
  NSString *TitleSearchStringStart = @"http://www.omdbapi.com/?t=";
  NSString *ShortPlotStringEnd = @"&y=&plot=short&r=json";
  NSString *LongPlotStringEnd = @"&y=&plot=full&r=json";
  NSMutableArray *current_movie_array = [[NSMutableArray alloc] init];
  search_queue = dispatch_queue_create("My first custom queue", DISPATCH_QUEUE_SERIAL);
  search_group = dispatch_group_create();
  NSString * Saved_Search_Query = @"";
  NSString * input = @"Harry";
  
  
  
  dispatch_sync(search_queue, ^{
    
    dispatch_group_enter(search_group);
    
    
    NSString *input_String = [BaseSearchURLStringStart stringByAppendingString:input];
    
    NSString *combined_String = [input_String stringByAppendingString:ShortPlotStringEnd];
    
    NSLog(@"Combined String is %@",combined_String);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:combined_String parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
      
      //Loop through all of the NSDictionary
      
      //Make sure that the object is not null
      
      NSString* response_valid = [responseObject objectForKey:@"Response"];
      
      
      
      if ( [response_valid isEqualToString:@"True"])
        {
        
        //We need to count the number of responses
        int result_count = [[responseObject objectForKey:@"totalResults"] intValue];
        int counter = 0;
        
        //The JSON only has at max 10 results so we need to set the result_count to 10 if the result_count is greater than 10
        if ( result_count > 0)
          {
          result_count = 10;
          }
        
        Movie* current_movie;
        
        while (counter < result_count)
          {
          
          current_movie = [[Movie alloc] init];
          
          
          //This is used to convert characters diacritic title strings to normal character strings.
          
          current_movie.title = [responseObject objectForKey:@"Search"][counter][@"Title"];
          
          current_movie.m_id = [responseObject objectForKey:@"Search"][counter][@"imdbID"];
          
          current_movie.image_URL = [responseObject objectForKey:@"Search"][counter][@"Poster"];
          
          current_movie.year = [responseObject objectForKey:@"Search"][counter][@"Year"];
          
          [current_movie_array addObject:current_movie];
          
          
          counter++;
          }
        
        }
      
      dispatch_group_leave(search_group);
      
      dispatch_group_notify(search_group, dispatch_get_main_queue(), ^{
        //After all of the images have downloaded
        //          completionBlock(nil);
        
        XCTAssertEqual(current_movie_array.count, 9);
        
        //        NSLog(@"The number of elements is %lu", (unsigned long)[current_movie_array count]);
        
      });
      
      
    } failure:^(NSURLSessionTask *operation, NSError *error) {
      NSLog(@"Error: %@", error);
      //        completionBlock(error);
      
      NSLog(@"There is an error %@",error);
    }];
    
    
    NSLog(@"The search string is %@",input);
    
  });

  
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
  [self measureBlock:^{
    
    }];
}




@end
